// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.


importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.1/firebase-messaging.js');


// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.

//
// firebase.initializeApp( {
//     apiKey: 'AIzaSyA8gxlVYlTUZ6QGolOkNQTiUwPLpCZyscQ',
//     authDomain: 'ikcheckapp-e400a.firebaseapp.com',
//     projectId: 'ikcheckapp-e400a',
//     storageBucket: 'ikcheckapp-e400a.appspot.com',
//
//     messagingSenderId: '846069705220',
//     appId: '1:846069705220:web:24486e3f3ab8e53a756351',
//     measurementId: 'G-KZK351ZY0D'
// })

firebase.initializeApp( {
apiKey: 'AIzaSyCIpagnSmBtcERvdyZMOxmkEmxYrEMgmYM',
    authDomain: 'ikcheck-acc-notification-test.firebaseapp.com',
    projectId: 'ikcheck-acc-notification-test',
    storageBucket: 'ikcheck-acc-notification-test.appspot.com',
    messagingSenderId: '562362987395',
    appId: '1:562362987395:web:f7734e8f0efae4625d3cb6',
    measurementId: 'G-L2C393V3SL'
})

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
if (firebase.messaging.isSupported()) {
    const messaging = firebase.messaging();

    messaging.onBackgroundMessage(function (payload) {
        console.log('recieved from background', payload);
        const clickUrl = payload.data["gcm.notification.url"];
        localStorage.setItem('US_fetch_messages', '' + true);
        let userUnreadMsgCount = +localStorage.getItem('US_unread_news_count') + 1;
        this.dataService.addMsgUnreadCount(userUnreadMsgCount);
        localStorage.setItem('US_unread_news_count', '' + userUnreadMsgCount);
        const notificationTitle = payload.notification.title;
        const notificationOptions = {
            body: payload.notification.body,
            icon: "https://acc.ikcheck.app/us-notification/us-nl-logo.png",
        };

        self.addEventListener("notificationclick", function (event) {
            event.notification.close();
            event.waitUntil(self.clients.openWindow(clickUrl));
        });

        return self.registration.showNotification(
            notificationTitle,
            notificationOptions
        );
    });
}

/*messaging.setBackgroundMessageHandler(function(payload) {
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
});*/


//-----------------------------------------------------------------------------------------

/*self.addEventListener('notificationclick', function(event) {
    console.log('On notification click: ', event.notification.data.url);
    // Android doesn’t close the notification when you click on it
    // See: http://crbug.com/463146
    event.notification.close();

    // This looks to see if the current is already open and
    // focuses if it is
    console.log('Notification click: data.url ', event.notification.data.url);
    event.notification.close();
    var url = /localhost:3999|dev-piquemeup.firebaseapp.com/;
    var newurl = "/chat";
    if (event.notification.data.url) {
        newurl = event.notification.data.url;
    }

    function endsWith(str, suffix) {
        return str.indexOf(suffix, str.length - suffix.length) !== -1;
    }

    event.waitUntil(
        clients.matchAll({
            type: 'window'
        })
            .then(function(windowClients) {
                for (var i = 0; i < windowClients.length; i++) {
                    var client = windowClients[i];
                    if (url.test(client.url) && 'focus' in client) {
                        if (endsWith(client.url, "/app.html#" + newurl)) {
                            console.log("******** Yes it matched *******");
                            return client.focus();
                        }
                        return client.navigate("/app.html#" + newurl)
                            .then(client => client.focus());
                    }
                }
                if (clients.openWindow) {
                    return clients.openWindow("/app.html#" + newurl);
                }
            })
    );

});*/

//===========================================================================================

// messaging.setBackgroundMessageHandler(function(payload) {
//     console.log('[firebase-messaging-sw.js] Received background message ', payload);
//     // Customize notification here
//     var notificationTitle = 'Background Message Title';
//     var notificationOptions = {
//         body: 'Background Message body.',
//         icon: '/firebase-logo.png',
//         sound: 'default'
//     };
//
//     return self.registration.showNotification(notificationTitle,
//         notificationOptions);
// });
